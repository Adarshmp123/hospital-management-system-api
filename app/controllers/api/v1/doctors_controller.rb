class Api::V1::DoctorsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:index, :show, :create]
  respond_to :json

  swagger_controller :doctors, 'Doctors'

      swagger_api :index do
        summary 'Returns all posts'
        notes 'Notes...'
      end
      swagger_api :create do
        summary 'Creating doctors'
        notes 'Should be used for creating doctors'
        param :form, 'doctor[name]', :string, :required, 'name'
        param :form, 'doctor[phone_number]', :string, :required, 'phone_number'
        param :form, 'doctor[specialization]', :string, :required, 'specialization'
      end


  def index
    @doctors= Doctor.select(:id, :name)
    render json: { success: true ,doctor: @doctors}
  end

  def show
    respond_with Doctor.find(params[:id])
  end

  def create
    @doctor=Doctor.new(doctor_params) 
    if @doctor.save 
      render json: @doctor, status: 201
    else
      render json: { errors: @doctor.errors}, status: 422
    end
  end

  private
  def doctor_params
    params.require(:doctor).permit(:name, :phone_number, :specialization) 
  end


end  