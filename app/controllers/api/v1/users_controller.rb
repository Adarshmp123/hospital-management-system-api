class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, only: [:update, :destroy]
  respond_to :json

  swagger_controller :users, 'Users'

      swagger_api :index do
        summary 'Returns all posts'
        notes 'Notes...'
      end

       swagger_api :create do
         summary 'Creating users'
         notes 'Should be used for creating users'
         param :form, 'user[email]', :string, :required, 'email'
         param :form, 'user[password]', :string, :required, 'password'
         param :form, 'user[password_confirmation]', :string, :required, 'password_confirmation'
      end

      def index
        @users = User.all

        render json: @users, status: :ok
      end

  def show
    respond_with User.find(params[:id]) 
  end

  def create
    user = User.new(user_params) 
    if user.save
      render json:{ success: user}, status: 201
    else
      render json: { errors: user.errors.full_messages }, status: 422
    end
  end

  def update
    user = current_user

    if user.update(user_params)
      render json: user, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
    current_user.destroy
    head 204
  end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation) 
    end
end
