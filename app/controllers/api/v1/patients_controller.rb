class Api::V1::PatientsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:index, :show, :create]

  respond_to :json

  swagger_controller :patients, 'Patients'

      swagger_api :index do
        summary 'Returns all posts'
        notes 'Notes...'
      end
      
      swagger_api :create do
         summary 'Creating patients'
         notes 'Should be used for creating patients'
         param :form, 'patient[name]', :string, :required, 'name'
         param :form, 'patient[phone_number]', :string, :required, 'phone_number'
      end


  def index
    @patients = Patient.select(:id, :name)
    render json: { success: true, patient: @patients}
  end

  def show
    respond_with Patient.find(params[:id])
  end

  def create
    @patient=Patient.new(patient_params) 
    if @patient.save 
      render json: @patient, status: 201
    else
      render json: { errors: @patient.errors}, status: 422
    end
  end

  private
  def patient_params
    params.require(:patient).permit(:name, :phone_number) 
  end
end  
