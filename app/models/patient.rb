class Patient < ApplicationRecord
  validates_presence_of :name, :phone_number
 
  has_many :appointments
end
