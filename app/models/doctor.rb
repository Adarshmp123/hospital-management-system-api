class Doctor < ApplicationRecord
  validates :name, presence: true
  validates :phone_number, presence: true
  validates :specialization, presence: true
  has_many :appointments
end
