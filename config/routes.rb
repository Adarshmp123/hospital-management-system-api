Rails.application.routes.draw do 
  get '/api' => redirect('/swagger/dist/index.html?url=/apidocs/api-docs.json')
  devise_for :users
  resource :patients
  resource :sessions
  resource :doctors
  resource :appointments
  namespace :api, defaults:{ format: :json }do
    namespace :v1 do
      resources :patients, :only => [:show, :create, :index]
      resources :doctors, :only => [:show, :create, :index]
      resources :appointments, :only => [:show, :create, :index]
      resources :users, :only => [:show, :create, :update, :destroy, :index]
      resources :sessions, :only => [:create, :destroy]
    end
  end 
end 