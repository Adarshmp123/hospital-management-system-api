class RemoveDocNameFromAppointments < ActiveRecord::Migration[5.0]
  def change
    remove_column :appointments, :doc_name
  end
end
